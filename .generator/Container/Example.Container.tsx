import './Example.Styles.scss';

import React, { Component } from 'react';

import { IRootReducer } from '../../Interface/IRootReducer';
import { InferProps } from 'prop-types';
import { connect } from 'react-redux';

type Props = {};

class Example extends Component<InferProps<Props>> {
    state = {};
    render(): JSX.Element {
        return <div className="example">Example container ready</div>;
    }
}

const mapStateToProps = (store: IRootReducer): Record<string, any> => ({});

const mapDispatchToProps = (dispatch: Function): Record<string, any> => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Example);
