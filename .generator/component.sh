name=$@
lowercase=$(echo "$@" | tr '[:upper:]' '[:lower:]')

cp -r ./.generator/Component ./.generator/temp/$name
mv ./.generator/temp/$name/Example.Component.tsx ./.generator/temp/$name/$name.Component.tsx
mv ./.generator/temp/$name/Example.Styles.scss ./.generator/temp/$name/$name.Styles.scss

find ./.generator/temp/$name -type f -name \*.Component.tsx -exec sed -i.bak 's|Example|'$name'|g' {} +
find ./.generator/temp/$name -type f -name \*.Component.tsx -exec sed -i.bak 's|example|'$lowercase'|g' {} +
find ./.generator/temp/$name -type f -name \*.Styles.scss -exec sed -i.bak 's|example|'$lowercase'|g' {} +
find ./.generator/temp/$name -name \*.bak -delete

mv ./.generator/temp/$name ./src/Component/$name