import './Example.Styles.scss';

import PropTypes, { InferProps } from 'prop-types';

import React from 'react';

const propTypes = {
    myProp: PropTypes.any,
};

const defaultProps = {
    myProp: null,
};

/**
 * @param {object} props
 * @param {any} props.myProp
 */
function ExampleComponent(props: InferProps<typeof propTypes>): JSX.Element {
    const { myProp } = props;

    return <div className="example">Example component ready</div>;
}

ExampleComponent.propTypes = propTypes;
ExampleComponent.defaultProps = defaultProps;

export default ExampleComponent;
