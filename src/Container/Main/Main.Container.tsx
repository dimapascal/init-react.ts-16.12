import './Main.Styles.scss';

import { IRootReducer } from '../../Interface/IRootReducer';
import { InferProps } from 'prop-types';
import React from 'react';
import UserComponent from '../../Component/User/User.Component';
import { connect } from 'react-redux';
import { setAuth } from '../../Redux/Auth.Reducer';

type Props = {
    setAuth: Function;
    authState: object;
};

class App extends React.Component<InferProps<Props>> {
    state = {};

    render(): JSX.Element {
        return (
            <div className="main">
                <UserComponent />
                <UserComponent />
                <UserComponent />
            </div>
        );
    }
}

const mapStateToProps = (store: IRootReducer): Record<string, any> => ({
    authState: store.authReducer,
});

const mapDispatchToProps = (dispatch: Function): Record<string, any> => ({
    setAuth: (value: any): void => dispatch(setAuth(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
