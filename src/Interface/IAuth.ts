export default interface IAuth {
    accessToken: string;
    refreshToken: string;
    expiresIn: string | number;
}
