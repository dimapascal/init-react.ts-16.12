import { IDispatchAction } from './IDispatchAction';
import { Reducer } from 'redux';
export interface IRootReducer {
    authReducer: Reducer<Record<string, any>, IDispatchAction>;
}
