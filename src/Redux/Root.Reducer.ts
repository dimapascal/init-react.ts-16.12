import { combineReducers, createStore } from 'redux';

import { authReducer } from './Auth.Reducer';

export const RootReducer = combineReducers({
    authReducer,
});

export function configureStore() {
    const store = createStore(RootReducer);

    return store;
}
