import { IDispatchAction } from '../Interface/IDispatchAction';
import { Reducer } from 'redux';

const initialState: Record<string, any> = {};

export enum AuthActionTypes {
    SET_AUTH = 'SET_AUTH_REDUCER_ACTION',
}

export function setAuth(value: any): IDispatchAction {
    return { type: AuthActionTypes.SET_AUTH, payload: value };
}

export const authReducer: Reducer<Record<string, any>, IDispatchAction> = (state = initialState, action) => {
    if (AuthActionTypes.SET_AUTH === action.type) {
        return { ...state, ...action.payload };
    }
    return state;
};
