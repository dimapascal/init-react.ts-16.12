import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import MainContainer from '../Container/Main/Main.Container';
import React from 'react';

export default function RootNavigator(): JSX.Element {
    return (
        <Router>
            <Switch>
                <Route path="/" exact>
                    <MainContainer />
                </Route>
            </Switch>
        </Router>
    );
}
