import './User.Styles.scss';

import PropTypes, { InferProps } from 'prop-types';

import React from 'react';

const propTypes = {
    myProp: PropTypes.any,
};

const defaultProps = {
    myProp: null,
};

/**
 * @param {object} props
 * @param {any} props.myProp
 */
function UserComponent(props: InferProps<typeof propTypes>): JSX.Element {
    const { myProp } = props;

    return <div className="user">User works!</div>;
}

UserComponent.propTypes = propTypes;
UserComponent.defaultProps = defaultProps;

export default UserComponent;
