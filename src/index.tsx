import './index.css';

import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import RootNavigator from './Navigation/Root.Navigator';
import { configureStore } from './Redux/Root.Reducer';

ReactDOM.render(
    <Provider store={configureStore()}>
        <RootNavigator />
    </Provider>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
