import { AxiosRequestConfig } from 'axios';
import IAuth from '../Interface/IAuth';
import { URLS_CONST } from '../Constant/urls.const';
import { create } from 'apisauce';

export class ApiService {
    private static baseURL = URLS_CONST.API;

    private static defaultHeaders = {
        'content-type': 'application/json',
    };

    static api = create({
        baseURL: ApiService.baseURL,
        headers: ApiService.defaultHeaders,
    });
    static authorization: IAuth | null;

    /**
     * @param {Object} auth
     * @param {string} auth.accessToken
     * @param {string} auth.refreshToken
     * @param {number|string} auth.expiresIn
     */
    static setAuth(auth: IAuth) {
        if (auth) {
            ApiService.authorization = auth;
            ApiService.api.setHeader('Authorization', `Bearer ${auth.accessToken}`);
        }
    }
    static deleteAuth() {
        ApiService.authorization = null;
    }

    static async refreshToken() {
        if (ApiService.authorization && ApiService.authorization.refreshToken) {
            const { refreshToken } = ApiService.authorization;
            const { data }: any = await ApiService.api.get(`${ApiService.baseURL}auth/refresh-token/${refreshToken}`);
            return data;
        }
        return false;
    }

    static async validate(response: any) {
        const { data, ok, status } = response;
        if (!ok) {
            if (status === 401) {
                if (ApiService.authorization && ApiService.authorization.refreshToken) {
                    const refreshResponse = await ApiService.refreshToken();
                    if (refreshResponse) {
                        const { url, method, params, headers } = response.config;
                        const copyApiService: any = ApiService;
                        const repeat = await copyApiService[method](url, params, headers);
                        return repeat;
                    }
                } else {
                    window.location.href = '/';
                    ApiService.deleteAuth();
                    return data;
                }
            }
            return data;
        }
    }

    public async get(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.get(url, data, { headers });
        return await ApiService.validate(response);
    }

    public async post(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.post(url, data, { headers });
        return await ApiService.validate(response);
    }

    public async put(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.put(url, data, { headers });
        return await ApiService.validate(response);
    }

    public async delete(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.delete(url, data, { headers });
        return await ApiService.validate(response);
    }

    public async patch(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.patch(url, data, { headers });
        return await ApiService.validate(response);
    }

    public async head(url: string, data?: Record<string, any>, headers?: AxiosRequestConfig) {
        const response: any = await ApiService.api.head(url, data, { headers });
        return await ApiService.validate(response);
    }
}
